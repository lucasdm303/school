﻿Imports OPCAutomation

Public Class clsOPC
    Private WithEvents oPol As New System.Timers.Timer
    Private arrAppToPlc(8) As Integer
    Private arrHandshake(2) As Integer
    Private arrPlcToApp(2) As Integer
    Private iGoodProducts As Integer
    Private iRejectedProducts As Integer
    Private oBakingTemperature As OPCAutomation.OPCItem
    Private oBakingTime As OPCAutomation.OPCItem
    Private oBatch As BEC.clsBatch
    Private oBox As OPCAutomation.OPCItem
    Private oCommandStart As OPCAutomation.OPCItem
    Private oGoodProducts As OPCAutomation.OPCItem
    Private ogrpApptoplc As OPCAutomation.OPCGroup
    Private ogrpHandshake As OPCAutomation.OPCGroup
    Private ogrpPlctoapp As OPCAutomation.OPCGroup
    Private oID As OPCAutomation.OPCItem
    Private oMaterial1 As OPCAutomation.OPCItem
    Private oMaterial2 As OPCAutomation.OPCItem
    Private oMaterial3 As OPCAutomation.OPCItem
    Private oQuantity As OPCAutomation.OPCItem
    Private oRejectedProducts As OPCAutomation.OPCItem
    Private oServer As OPCAutomation.OPCServer
    Private oStateReady As OPCAutomation.OPCItem
    Private xStateReady As Boolean

    Public Event BatchReady(sender As Object, e As EventArgs)

    Public Sub New()
        oServer = New OPCServer
        oServer.Connect("Kepware.KEPServerEX.V5")
        ogrpApptoplc = oServer.OPCGroups.Add("AppToPlc")
        oID = ogrpApptoplc.OPCItems.AddItem(My.Resources.iID, 1)
        oMaterial1 = ogrpApptoplc.OPCItems.AddItem(My.Resources.iMaterial1, 2)
        oMaterial2 = ogrpApptoplc.OPCItems.AddItem(My.Resources.iMaterial2, 3)
        oMaterial3 = ogrpApptoplc.OPCItems.AddItem(My.Resources.iMaterial3, 4)
        oBakingTemperature = ogrpApptoplc.OPCItems.AddItem(My.Resources.iBakingTemperature, 5)
        oBakingTime = ogrpApptoplc.OPCItems.AddItem(My.Resources.iBakingTime, 6)
        oBox = ogrpApptoplc.OPCItems.AddItem(My.Resources.iBox, 7)
        oQuantity = ogrpApptoplc.OPCItems.AddItem(My.Resources.iQuantity, 8)
        arrAppToPlc(1) = oID.ServerHandle
        arrAppToPlc(2) = oMaterial1.ServerHandle
        arrAppToPlc(3) = oMaterial2.ServerHandle
        arrAppToPlc(4) = oMaterial3.ServerHandle
        arrAppToPlc(5) = oBakingTemperature.ServerHandle
        arrAppToPlc(6) = oBakingTime.ServerHandle
        arrAppToPlc(7) = oBox.ServerHandle
        arrAppToPlc(8) = oQuantity.ServerHandle

        ogrpPlctoapp = oServer.OPCGroups.Add("PlcToApp")
        oGoodProducts = ogrpPlctoapp.OPCItems.AddItem(My.Resources.iGoodProducts, 11)
        oRejectedProducts = ogrpPlctoapp.OPCItems.AddItem(My.Resources.iRejectedProducts, 12)
        arrPlcToApp(1) = oGoodProducts.ServerHandle
        arrPlcToApp(2) = oRejectedProducts.ServerHandle

        ogrpHandshake = oServer.OPCGroups.Add("Handshake")
        oCommandStart = ogrpHandshake.OPCItems.AddItem(My.Resources.iGoodProducts, 21)
        oStateReady = ogrpHandshake.OPCItems.AddItem(My.Resources.iRejectedProducts, 22)
        arrHandshake(1) = oCommandStart.ServerHandle
        arrHandshake(2) = oStateReady.ServerHandle

        oPol.Interval = 100
        oPol.Start()
    End Sub

    Public Sub Disconnect()
        oPol.Stop()
        oServer.OPCGroups.RemoveAll()
        oServer.Disconnect()
    End Sub

    Public Sub FinishBatch()

    End Sub

    Public Sub StartNewBatch(aBatch As BEC.clsBatch)
        oBatch = aBatch
        Dim arrErrors As Array
        Dim arrValues(8) As Object
        arrValues(1) = oBatch.Id
        arrValues(2) = oBatch.Mat1
        arrValues(3) = oBatch.Mat2
        arrValues(4) = oBatch.Mat3
        arrValues(5) = oBatch.BakingTemperature
        arrValues(6) = oBatch.BakingTime
        arrValues(7) = oBatch.Box
        arrValues(8) = oBatch.Quantity
        ogrpApptoplc.SyncWrite(8, arrAppToPlc, arrValues, arrErrors)
        oCommandStart.Write(True)
    End Sub

    Private Sub oPol_Elapsed(sender As Object, e As EventArgs) Handles oPol.Elapsed
        oPol.Stop()
        oStateReady.Read(OPCDataSource.OPCDevice)
        xStateReady = oStateReady.Value
        If (xStateReady = True) Then
            oStateReady.Write(0)
            Dim arrValues As Array
            Dim arrErrors As Array
            ogrpPlctoapp.SyncRead(OPCDataSource.OPCDevice, 2, arrPlcToApp, arrValues, arrErrors)
            iGoodProducts = arrValues(1)
            iRejectedProducts = arrValues(2)
            oBatch.GoodProducts = iGoodProducts
            oBatch.RejectedProducts = iRejectedProducts
            RaiseEvent BatchReady(Me, New EventArgs)
        End If
        oPol.Start()
    End Sub

    Public ReadOnly Property Batch As BEC.clsBatch
        Get
            Return oBatch
        End Get
    End Property
End Class