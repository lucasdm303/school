﻿Imports MySql.Data.MySqlClient

Public Class clsSql
    Private oConn As MySqlConnection
    Private hBox As New Hashtable

    Public Event NewOrders(send As Object, e As EventArgs)

    Public Sub New()
        oConn = New MySqlConnection(My.Resources.sqlstring)
        oConn.Open()
        Dim sQuery As String = "SELECT boid, boname FROM box"
        Dim sCmd As New MySqlCommand(sQuery, oConn)
        Dim sAdapter As New MySqlDataAdapter(sCmd)
        Dim oDTable As New DataTable
        sAdapter.Fill(oDTable)
        oConn.Close()
        For Each r As DataRow In oDTable.Rows
            Dim oBox As New BEC.clsBox(r)
            hBox.Add(oBox.Name, oBox)
        Next
    End Sub

    Public Sub ImportRequests(lstRequest As List(Of BEC.clsRequest))
        oConn.Open()
        For Each oRequest As BEC.clsRequest In lstRequest
            'CHECK IF EXISTS
            Dim sQuery As String = "SELECT cuid FROM customer WHERE cuname = (?cuname)"
            Dim sCmd As New MySqlCommand(sQuery, oConn)
            sCmd.Parameters.Add(New MySqlParameter("?cuname", oRequest.CustomerName))
            Dim iResult As Integer = sCmd.ExecuteScalar
            'INSERT
            If (iResult = 0) Then
                sQuery = "INSERT INTO customer (cuname) VALUES (?cuname)"
                sCmd = New MySqlCommand(sQuery, oConn)
                sCmd.Parameters.Add(New MySqlParameter("?cuname", oRequest.CustomerName))
                sCmd.ExecuteNonQuery()
                sQuery = "SELECT cuid FROM customer WHERE cuname = (?cuname)"
                sCmd = New MySqlCommand(sQuery, oConn)
                sCmd.Parameters.Add(New MySqlParameter("?cuname", oRequest.CustomerName))
                sCmd.ExecuteScalar()
                sCmd = New MySqlCommand(sQuery, oConn)
                sCmd.Parameters.Add(New MySqlParameter("?cuname", oRequest.CustomerName))
                iResult = sCmd.ExecuteScalar
            End If
            sQuery = "INSERT INTO productionrequest(pqNumber, pqcuCustomer, pqEarliestStartTime, pqLatestEndTime) 
                                        VALUES (?pqNumber, ?pqcuCustomer, ?pqEarliestStartTime, ?pqLatestEndTime)"
            sCmd = New MySqlCommand(sQuery, oConn)
            sCmd.Parameters.Add(New MySqlParameter("?pqNumber", oRequest.No))
            sCmd.Parameters.Add(New MySqlParameter("?pqcuCustomer", iResult))
            sCmd.Parameters.Add(New MySqlParameter("?pqEarliestStartTime", oRequest.EarliestStartTime))
            sCmd.Parameters.Add(New MySqlParameter("?pqLatestEndTime", oRequest.LastEndTime))
            sCmd.ExecuteNonQuery()
            For Each oRequirement As BEC.clsRequirement In oRequest.Requirements
                Dim oBox As BEC.clsBox = hBox.Item(oRequirement.Box)
                'SEARCH PRODUCT ID
                sQuery = "SELECT prId FROM Product WHERE prName = (?prName)"
                sCmd = New MySqlCommand(sQuery, oConn)
                sCmd.Parameters.Add(New MySqlParameter("?prName", oRequirement.Product))
                Dim iProdID As Integer = sCmd.ExecuteScalar
                'SEARCH REQUEST ID
                sQuery = "SELECT pqId FROM productionrequest WHERE pqNumber = (?pqNumber)"
                sCmd = New MySqlCommand(sQuery, oConn)
                sCmd.Parameters.Add(New MySqlParameter("?pqNumber", oRequest.No))
                Dim iRequestID As Integer = sCmd.ExecuteScalar
                sQuery = "INSERT INTO requirement (rqpqProductionRequest, rqprProduct, rqboBox, rqQuantity) VALUES 
                                                    (?rqpqProductionRequest, ?rqprProduct, ?rqboBox, ?rqQuantity)"
                sCmd = New MySqlCommand(sQuery, oConn)
                sCmd.Parameters.Add(New MySqlParameter("?rqpqProductionRequest", iRequestID))
                sCmd.Parameters.Add(New MySqlParameter("?rqprProduct", iProdID))
                sCmd.Parameters.Add(New MySqlParameter("?rqboBox", oBox.Id))
                sCmd.Parameters.Add(New MySqlParameter("?rqQuantity", oRequirement.Quantity))
                sCmd.ExecuteNonQuery()
            Next
        Next
        oConn.Close()
        RaiseEvent NewOrders(Me, New EventArgs)
    End Sub

    Public Function GetBatchScheduling() As DataTable
        Dim oDTable As New DataTable
        oConn.Open()
        Dim sQuery As String = "SELECT cuname, pqnumber FROM customer
                                JOIN productionrequest ON productionrequest.pqId = customer.cuId"
        'select cuname, pqnumber,rqquantity from customer join productionrequest on pqcucustomer = cuid join requirement on pqid = rqpqproductionrequest
        '"JOIN requirement ON requirement.rqId = productionrequest.pqId"
        Dim sCmd As New MySqlCommand(sQuery, oConn)
        Dim sAdapter As New MySqlDataAdapter(sCmd)
        sAdapter.Fill(oDTable)
        oConn.Close()
        Return oDTable
    End Function

    Public Function GetNewBatch() As BEC.clsBatch
        Dim oDTable As New DataTable
        Dim oBatch As BEC.clsBatch
        oConn.Open()
        Dim sQuery As String = "SELECT rqId, rqboBox, rqQuantity, prBakingTemperature, 
                                prBakingTime, remaMaterial, reUnits 
                                FROM requirement
                                JOIN product ON rqprProduct = prId
                                JOIN recipe ON prId = reprProduct
                                WHERE rqBusy = 0 AND rqDone = 0
                                ORDER BY rqId, remaMaterial
                                LIMIT 3"
        Dim sCmd As New MySqlCommand(sQuery, oConn)
        Dim sAdapter As New MySqlDataAdapter(sCmd)
        sAdapter.Fill(oDTable)
        If (oDTable.Rows.Count >= 3) Then
            oBatch = New BEC.clsBatch(oDTable)
            sQuery = "UPDATE requirement 
                      SET rqBusy = 1, rqStarttime = ?rqstarttime
                      WHERE rqId = ?rqid"
            sCmd = New MySqlCommand(sQuery, oConn)
            sCmd.Parameters.Add(New MySqlParameter("?rqId", oBatch.Id))
            sCmd.Parameters.Add(New MySqlParameter("?rqId", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")))
            sCmd.ExecuteNonQuery()
        End If
        oConn.Close()
        Return oBatch
    End Function

    Public Sub FinishBatch(oBatch As BEC.clsBatch)
        oConn.Open()
        Dim sQuery = "UPDATE requirement 
                      SET rqbusy = 0, rqDone = 1, rqEndTime = ?rqEndTime,
                      rqGoodProducts = ?rqGoodProducts, 
                      rqRejectedProducts = ?rqRejectedProducts
                      WHERE rqId = (?rqId)"
        Dim sCmd = New MySqlCommand(sQuery, oConn)
        sCmd.Parameters.Add(New MySqlParameter("?rqId", oBatch.Id))
        sCmd.Parameters.Add(New MySqlParameter("?rqEndTime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")))
        sCmd.Parameters.Add(New MySqlParameter("?rqGoodProducts", oBatch.GoodProducts))
        sCmd.Parameters.Add(New MySqlParameter("?rqRejectedProducts", oBatch.RejectedProducts))
        sCmd.ExecuteNonQuery()
        oConn.Close()
    End Sub
End Class