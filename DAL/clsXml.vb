﻿Imports System.Xml

Public Class clsXml
    Private lstRequest As New List(Of BEC.clsRequest)

    Public ReadOnly Property getRequests As List(Of BEC.clsRequest)
        Get
            Return lstRequest
        End Get
    End Property

    Public Sub New(strPath As String)
        Dim xDoc As New XmlDocument
        xDoc.Load(strPath)
        Dim xlstRequests As XmlNodeList
        xlstRequests = xDoc.SelectNodes("SapGantt/ProductionSchedule/ProductionRequests/ProductionRequest")
        For Each xElement As XmlElement In xlstRequests
            Dim oRequest As New BEC.clsRequest(xElement)
            lstRequest.Add(oRequest)
        Next
    End Sub

End Class
