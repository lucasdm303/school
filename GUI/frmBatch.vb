﻿Public Class frmBatch
    Private oSql As New DAL.clsSql

    Public Sub New(sSql As DAL.clsSql)
        InitializeComponent()
        oSql = sSql
    End Sub

    Private Sub frmBatch_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dGrid.DataSource = oSql.GetBatchScheduling()
    End Sub
End Class