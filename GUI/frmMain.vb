﻿Public Class frmMain
    Private WithEvents oSql As DAL.clsSql
    Private WithEvents oClient As DAL.clsOPC

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        oSql = New DAL.clsSql
        oClient = New DAL.clsOPC
    End Sub

    Private Sub frmMain_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        For Each frm As Form In Me.MdiChildren
            frm.Close()
        Next
    End Sub

    Private Sub ImportRequestsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ImportRequestsToolStripMenuItem.Click
        Dim frm As New frmRequests(oSql)
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub CreateResponseToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CreateResponseToolStripMenuItem.Click
        Dim frm As New frmResponse
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub BatchSchedulingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BatchSchedulingToolStripMenuItem.Click
        Dim frm As New frmBatch(oSql)
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private xWait As Boolean
    Private Sub oClient_BatchReady(sender As Object, e As EventArgs) Handles oClient.BatchReady
        If oClient.Batch IsNot Nothing Then
            oSql.FinishBatch(oClient.Batch)
        End If
        Dim aBatch As BEC.clsBatch = oSql.GetNewBatch
        If aBatch IsNot Nothing Then
            oClient.StartNewBatch(aBatch)
        Else
            xWait = True
        End If
    End Sub

    Private Sub oSql_NewOrders(send As Object, e As EventArgs) Handles oSql.NewOrders
        If (xWait = True) Then
            xWait = False
            Dim oBatch As BEC.clsBatch = oSql.GetNewBatch
            If oBatch IsNot Nothing Then
                oClient.StartNewBatch(oBatch)
            End If
        End If
    End Sub
End Class