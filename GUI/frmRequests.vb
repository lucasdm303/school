﻿Public Class frmRequests
    Private oSql As New DAL.clsSql

    Public Sub New(sSql As DAL.clsSql)
        InitializeComponent()
        oSql = sSql
    End Sub

    Private Sub frmRequests_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtbCustomerName.Enabled = False
        txtbEarliest.Enabled = False
        txtbLatest.Enabled = False
    End Sub

    Private Sub btnImport_Click(sender As Object, e As EventArgs) Handles btnImport.Click
        Dim oFileDialog As New OpenFileDialog
        Dim oDialogResult As DialogResult = oFileDialog.ShowDialog
        If (oDialogResult.OK) Then
            Dim oRequest As New DAL.clsXml(oFileDialog.FileName)
            lstbRequests.DataSource = oRequest.getRequests
            oSql.ImportRequests(oRequest.getRequests)
        End If
    End Sub

    Private Sub lstbRequests_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstbRequests.SelectedIndexChanged
        Dim oRequest As BEC.clsRequest = lstbRequests.SelectedItem
        txtbCustomerName.Text = oRequest.CustomerName
        txtbEarliest.Text = oRequest.EarliestStartTime
        txtbLatest.Text = oRequest.LastEndTime
        lstbRequirements.DataSource = oRequest.Requirements
    End Sub
End Class