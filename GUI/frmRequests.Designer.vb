﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRequests
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstbRequests = New System.Windows.Forms.ListBox()
        Me.lstbRequirements = New System.Windows.Forms.ListBox()
        Me.txtbCustomerName = New System.Windows.Forms.TextBox()
        Me.txtbEarliest = New System.Windows.Forms.TextBox()
        Me.txtbLatest = New System.Windows.Forms.TextBox()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lstbRequests
        '
        Me.lstbRequests.FormattingEnabled = True
        Me.lstbRequests.Location = New System.Drawing.Point(12, 12)
        Me.lstbRequests.Name = "lstbRequests"
        Me.lstbRequests.Size = New System.Drawing.Size(215, 212)
        Me.lstbRequests.TabIndex = 0
        '
        'lstbRequirements
        '
        Me.lstbRequirements.FormattingEnabled = True
        Me.lstbRequirements.Location = New System.Drawing.Point(233, 90)
        Me.lstbRequirements.Name = "lstbRequirements"
        Me.lstbRequirements.Size = New System.Drawing.Size(288, 134)
        Me.lstbRequirements.TabIndex = 1
        '
        'txtbCustomerName
        '
        Me.txtbCustomerName.Enabled = False
        Me.txtbCustomerName.Location = New System.Drawing.Point(233, 12)
        Me.txtbCustomerName.Name = "txtbCustomerName"
        Me.txtbCustomerName.Size = New System.Drawing.Size(288, 20)
        Me.txtbCustomerName.TabIndex = 2
        '
        'txtbEarliest
        '
        Me.txtbEarliest.Enabled = False
        Me.txtbEarliest.Location = New System.Drawing.Point(233, 38)
        Me.txtbEarliest.Name = "txtbEarliest"
        Me.txtbEarliest.Size = New System.Drawing.Size(288, 20)
        Me.txtbEarliest.TabIndex = 3
        '
        'txtbLatest
        '
        Me.txtbLatest.Enabled = False
        Me.txtbLatest.Location = New System.Drawing.Point(233, 64)
        Me.txtbLatest.Name = "txtbLatest"
        Me.txtbLatest.Size = New System.Drawing.Size(288, 20)
        Me.txtbLatest.TabIndex = 4
        '
        'btnImport
        '
        Me.btnImport.Location = New System.Drawing.Point(446, 240)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(75, 23)
        Me.btnImport.TabIndex = 5
        Me.btnImport.Text = "Import"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'frmRequests
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(533, 275)
        Me.Controls.Add(Me.btnImport)
        Me.Controls.Add(Me.txtbLatest)
        Me.Controls.Add(Me.txtbEarliest)
        Me.Controls.Add(Me.txtbCustomerName)
        Me.Controls.Add(Me.lstbRequirements)
        Me.Controls.Add(Me.lstbRequests)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmRequests"
        Me.Text = "Production Requests"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstbRequests As ListBox
    Friend WithEvents lstbRequirements As ListBox
    Friend WithEvents txtbCustomerName As TextBox
    Friend WithEvents txtbEarliest As TextBox
    Friend WithEvents txtbLatest As TextBox
    Friend WithEvents btnImport As Button
End Class
