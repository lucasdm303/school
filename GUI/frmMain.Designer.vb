﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ImportRequestsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreateResponseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BatchSchedulingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ImportRequestsToolStripMenuItem, Me.BatchSchedulingToolStripMenuItem, Me.CreateResponseToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(984, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "mStrip"
        '
        'ImportRequestsToolStripMenuItem
        '
        Me.ImportRequestsToolStripMenuItem.Name = "ImportRequestsToolStripMenuItem"
        Me.ImportRequestsToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.ImportRequestsToolStripMenuItem.Text = "Import Requests"
        '
        'CreateResponseToolStripMenuItem
        '
        Me.CreateResponseToolStripMenuItem.Name = "CreateResponseToolStripMenuItem"
        Me.CreateResponseToolStripMenuItem.Size = New System.Drawing.Size(106, 20)
        Me.CreateResponseToolStripMenuItem.Text = "Create Response"
        '
        'BatchSchedulingToolStripMenuItem
        '
        Me.BatchSchedulingToolStripMenuItem.Name = "BatchSchedulingToolStripMenuItem"
        Me.BatchSchedulingToolStripMenuItem.Size = New System.Drawing.Size(111, 20)
        Me.BatchSchedulingToolStripMenuItem.Text = "Batch Scheduling"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 712)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "MyMes - LDM"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ImportRequestsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CreateResponseToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BatchSchedulingToolStripMenuItem As ToolStripMenuItem
End Class
