﻿Imports System.Xml

Public Class clsRequirement
    Private iNo As Integer
    Private strProduct As String
    Private strBox As String
    Private iQuantity As Integer

    Public Sub New(xElement As XmlElement)
        iNo = xElement.SelectSingleNode("No").InnerText
        strProduct = xElement.SelectSingleNode("Product").InnerText
        strBox = xElement.SelectSingleNode("Box").InnerText
        iQuantity = xElement.SelectSingleNode("Quantity").InnerText
    End Sub

    Public ReadOnly Property No As Integer
        Get
            Return iNo
        End Get
    End Property

    Public ReadOnly Property Quantity As Integer
        Get
            Return iQuantity
        End Get
    End Property

    Public ReadOnly Property Box As String
        Get
            Return strBox
        End Get
    End Property

    Public ReadOnly Property Product As String
        Get
            Return strProduct
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return iNo
    End Function

End Class
