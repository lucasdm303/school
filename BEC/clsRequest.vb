﻿Imports System.Xml

Public Class clsRequest
    Private iNo As Integer
    Private strCustomerName As String
    Private strEarliest As String
    Private strLast As String
    Private lstRequirement As New List(Of BEC.clsRequirement)

    Public Sub New(xElement As XmlElement)
        iNo = xElement.SelectSingleNode("No").InnerText
        strCustomerName = xElement.SelectSingleNode("CustomerName").InnerText
        strEarliest = xElement.SelectSingleNode("EarliestStartTime").InnerText
        strLast = xElement.SelectSingleNode("LatestEndTime").InnerText
        Dim xlstRequirement As XmlNodeList
        xlstRequirement = xElement.SelectNodes("Requirements/Requirement")
        For Each xRequirement As XmlElement In xlstRequirement
            Dim oRequirement As New BEC.clsRequirement(xRequirement)
            lstRequirement.Add(oRequirement)
        Next
    End Sub

    Public ReadOnly Property No As Integer
        Get
            Return iNo
        End Get
    End Property

    Public ReadOnly Property CustomerName As String
        Get
            Return strCustomerName
        End Get
    End Property

    Public ReadOnly Property EarliestStartTime As String
        Get
            Return strEarliest
        End Get
    End Property

    Public ReadOnly Property LastEndTime As String
        Get
            Return strLast
        End Get
    End Property

    Public ReadOnly Property Requirements As List(Of BEC.clsRequirement)
        Get
            Return lstRequirement
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return iNo
    End Function

End Class
