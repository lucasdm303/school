﻿Public Class clsBatch
    Private iBakingTemperature As Integer
    Private iBakingTime As Integer
    Private iBox As Integer
    Private iGoodProducts As Integer
    Private iId As Integer
    Private iMat1 As Integer
    Private iMat2 As Integer
    Private iMat3 As Integer
    Private iQuantity As Integer
    Private iRejectedProducts As Integer

    Public Sub New(oBatch As DataTable)
        iId = oBatch.Rows(0).Item(0)
        iBox = oBatch.Rows(0).Item(1)
        iQuantity = oBatch.Rows(0).Item(2)
        iBakingTemperature = oBatch.Rows(0).Item(3)
        iBakingTime = oBatch.Rows(0).Item(4)
        iMat1 = oBatch.Rows(0).Item(6)
        iMat2 = oBatch.Rows(1).Item(6)
        iMat3 = oBatch.Rows(2).Item(6)
    End Sub

    Public ReadOnly Property BakingTemperature As Integer
        Get
            Return iBakingTemperature
        End Get
    End Property

    Public ReadOnly Property BakingTime As Integer
        Get
            Return iBakingTime
        End Get
    End Property

    Public ReadOnly Property Box As Integer
        Get
            Return iBox
        End Get
    End Property

    Public Property GoodProducts As Integer
        Get
            Return iGoodProducts
        End Get
        Set(value As Integer)
            iGoodProducts = value
        End Set
    End Property

    Public ReadOnly Property Id As Integer
        Get
            Return iId
        End Get
    End Property

    Public ReadOnly Property Mat1 As Integer
        Get
            Return iMat1
        End Get
    End Property

    Public ReadOnly Property Mat2 As Integer
        Get
            Return iMat2
        End Get
    End Property

    Public ReadOnly Property Mat3 As Integer
        Get
            Return iMat3
        End Get
    End Property

    Public ReadOnly Property Quantity As Integer
        Get
            Return iQuantity
        End Get
    End Property

    Public Property RejectedProducts As Integer
        Get
            Return iRejectedProducts
        End Get
        Set(value As Integer)
            iRejectedProducts = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return iId
    End Function

End Class