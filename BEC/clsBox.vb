﻿Public Class clsBox
    Private iID As Integer
    Private strName As String

    Public Sub New(obox As DataRow)
        iID = obox.Item(0)
        strName = obox.Item(1)
    End Sub

    Public ReadOnly Property Id As Integer
        Get
            Return iID
        End Get
    End Property

    Public ReadOnly Property Name As String
        Get
            Return strName
        End Get
    End Property

End Class
